#include "bst.hpp"
#include <algorithm>
#include <limits>
#include <queue>
#include <iostream>

namespace john {

BST::BST() : root_(nullptr) {

}

void BST::DeleteTree(TreeNode *current) {
    if (current == nullptr) {
        return;
    }

    if (current->GetLeft() != nullptr) {
        DeleteTree(current->GetLeft());
    }

    if (current->GetRight() != nullptr) {
        DeleteTree(current->GetRight());
    }

    delete current;
}

BST::~BST() {
    DeleteTree(root_);
}

TreeNode* BST::Insert(int value, TreeNode *current) {
    if (current == nullptr) {
        current = new TreeNode(value, nullptr, nullptr);
    } else if (value < current->GetData()) {
        current->SetLeft(Insert(value, current->GetLeft()));
    } else if (value > current->GetData()) {
        current->SetRight(Insert(value, current->GetRight()));
    }

    return current;
}

void BST::Insert(int value) {
    root_ = Insert(value, root_);
}


bool BST::Search(int value, TreeNode *current) {
    if (current == nullptr) {
        return false;
    } else if (value < current->GetData()) {
        return Search(value, current->GetLeft());
    } else if (value > current->GetData()) {
        return Search(value, current->GetRight());
    } else {
        return true;
    }
}

bool BST::Search(int value) {
    return Search(value, root_);
}

TreeNode* BST::Delete(int value, TreeNode *current) {
    if (current == nullptr) {
        return nullptr;
    } 

    if (value < current->GetData()) {
        current->SetLeft(Delete(value, current->GetLeft()));
    } else if (value > current->GetData()) {
        current->SetRight(Delete(value, current->GetRight()));
    } else { // Found the node to delete
        // Case 1: Node has no children
        if (current->GetLeft() == nullptr && current->GetRight() == nullptr) {
            delete current;
            current = nullptr;
        }
        // Case 2: Node has one child
        else if (current->GetLeft() == nullptr) {
            TreeNode* temp = current;
            current = current->GetRight();
            delete temp;
        }
        else if (current->GetRight() == nullptr) {
            TreeNode* temp = current;
            current = current->GetLeft();
            delete temp;
        }
        // Case 3: Node has two children
        else {
            // Get min value in right subtree
            int min_value = GetMin(current->GetRight());
            // Set current node's value to minimum value
            current->SetData(min_value);
            // Delete duplicate node in right subtree
            current->SetRight(Delete(min_value, current->GetRight()));
        }
    }

    return current;
}

void BST::Delete(int value) {
    root_ = Delete(value, root_);
}

int BST::GetMin(TreeNode* current) {
    if (current == nullptr) {
        return -1;
    } else if (current->GetLeft() == nullptr) {
        return current->GetData();
    } else {
        return GetMin(current->GetLeft());
    }
}

int BST::GetMin() {
    return GetMin(root_);
}

int BST::GetMax(TreeNode* current) {
    if (current == nullptr) {
        return -1;
    } else if (current->GetRight() == nullptr) {
        return current->GetData();
    } else {
        return GetMax(current->GetRight());
    }
}

int BST::GetMax() {
    return GetMax(root_);
}

int BST::GetHeight(TreeNode *current) {
    if (current == nullptr) {
        return 0;
    }

    return 1 + std::max(GetHeight(current->GetLeft()), GetHeight(current->GetRight()));
}

int BST::GetHeight() {
    return GetHeight(root_);
}

int BST::GetSuccessor(TreeNode *current, int value) {
    if (current == nullptr) {
        return -1;
    }

    // Find target node
    TreeNode *target = current;
    while(target != nullptr) {
        if (value < target->GetData()) {
            target = target->GetLeft();
        } else if (value > target->GetData()) {
            target = target->GetRight();
        } else {
            break;
        }
    }

    if (target == nullptr) {
        return -1;
    }

    if (target->GetRight() != nullptr) {
        // Find min in current node's right subtree
        return GetMin(target->GetRight());
    } else {
        // Find deepest ancestor in current node's left subtree
        TreeNode* ancestor = current;
        TreeNode* successor = nullptr;

        while(ancestor != nullptr) {
            if (value < ancestor->GetData()) {
                successor = ancestor;
                ancestor = ancestor->GetLeft();
            } else if (value > ancestor->GetData()) {
                ancestor = ancestor->GetRight();
            } else {
                break;
            }
        }

        if (successor == nullptr) { // When target node is the max node
            return -1;
        } else {
            return successor->GetData();
        }
    }

}

int BST::GetSuccessor(int value) {
    return GetSuccessor(root_, value);
}

bool is_between(TreeNode *current, int min, int max) {
    if (current == nullptr) {
        return true;
    }

    if (current->GetData() < min || current->GetData() > max) {
        return false;
    }

    return is_between(current->GetLeft(), min, current->GetData()) &&
        is_between(current->GetRight(), current->GetData(), max);
}

bool is_bst(TreeNode *current) {
    return is_between(current, std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
}

bool BST::IsBST() {
    return is_between(root_, std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
}

void BST::PrintLevelOrder(TreeNode *current) {
    if (current == nullptr) {
        return;
    }

    std::queue<TreeNode*> Q;
    Q.push(current);

    TreeNode* temp;

    while(!Q.empty()) {
        temp = Q.front();
        std::cout << temp->GetData() << " ";
        
        Q.pop();

        if (temp->GetLeft() != nullptr) {
            Q.push(temp->GetLeft());
        }
        if (temp->GetRight() != nullptr) {
            Q.push(temp->GetRight());
        }
    }

    std::cout << "\n";
}

void BST::PrintLevelOrder() {
    PrintLevelOrder(root_);
}

void BST::PrintInOrder(TreeNode *current) {
    if (current->GetLeft() != nullptr) {
        PrintInOrder(current->GetLeft());
    }
    std::cout << current->GetData() << " ";
    if (current->GetRight() != nullptr) {
        PrintInOrder(current->GetRight());
    }
}

void BST::PrintInOrder() {
    PrintInOrder(root_);
    std::cout << "\n";
}

}
