#ifndef TREE_NODE_HPP
#define TREE_NODE_HPP

namespace john {

class TreeNode {
private:
    int data_;
    TreeNode *left_;
    TreeNode *right_;

public:
    TreeNode(int data, TreeNode *left, TreeNode *right) : data_(data), left_(left), right_(right) {}
    int GetData() { return data_; }
    void SetData(int data) { data_ = data; }
    TreeNode* GetLeft() { return left_; }
    void SetLeft(TreeNode* left) { left_ = left; }
    TreeNode* GetRight() { return right_; }
    void SetRight(TreeNode* right) { right_ = right; }
};

}

#endif