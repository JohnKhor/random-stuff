#ifndef BST_HPP
#define BST_HPP

#include "tree_node.hpp"

namespace john {

class BST {
private:
    TreeNode *root_;

    void DeleteTree(TreeNode *current);

    TreeNode* Insert(int value, TreeNode *current);
    bool Search(int value, TreeNode *current);
    TreeNode* Delete(int value, TreeNode *current);
    
    int GetMin(TreeNode *current);
    int GetMax(TreeNode *current);
    int GetHeight(TreeNode *current);
    int GetSuccessor(TreeNode *current, int value);
    
    void PrintLevelOrder(TreeNode *current);
    void PrintInOrder(TreeNode *current);

public:
    BST();
    ~BST();
    
    void Insert(int value);
    bool Search(int value);
    void Delete(int value);

    int GetMin();
    int GetMax();
    int GetHeight();
    int GetSuccessor(int value);
    bool IsBST();

    void PrintLevelOrder();
    void PrintInOrder();
};

bool is_between(TreeNode *current, int min, int max);
bool is_bst(TreeNode *current);

}

#endif