#include "bst.hpp"
#include "bst.cpp"
#include <cassert>

int main() {
    john::BST tree;

    //       8
    //     /   \
    //    3    10
    //  /  \     \
    // 1    6    14
    //    /  \   /
    //   4   7  13
    // Insert
    tree.Insert(8);
    tree.Insert(3);
    tree.Insert(10);
    tree.Insert(1);
    tree.Insert(6);
    tree.Insert(14);
    tree.Insert(4);
    tree.Insert(7);
    tree.Insert(13);

    // Search
    assert(tree.Search(7));
    assert(tree.Search(6));
    assert(tree.Search(14));
    assert(!tree.Search(40));

    // Min, max and height
    int min = tree.GetMin();
    int max = tree.GetMax();
    int height = tree.GetHeight();

    std::cout << "Min: " << min << "\n";
    assert(min == 1);

    std::cout << "Max: " << max << "\n";
    assert(max == 14);

    std::cout << "Height: " << height << "\n";
    assert(height == 4);

    // Successor
    int successor = tree.GetSuccessor(3);
    std::cout << "Successor of 3 is " << successor << "\n";
    assert(successor == 4); // Target node has right subtree

    successor = tree.GetSuccessor(7);
    std::cout << "Successor of 7 is " << successor << "\n";
    assert(successor == 8); // Target node doesn't have right subtree

    successor = tree.GetSuccessor(14);
    std::cout << "Successor of 14 is " << successor << "\n";
    assert(successor == -1); // Max node

    // Print
    std::cout << "Tree items (level order): ";
    tree.PrintLevelOrder();
    
    std::cout << "Tree items (in order): ";
    tree.PrintInOrder();

    // IsBST
    assert(tree.IsBST());

    //    30
    //   /  \
    //  20  40
    //   \
    //   10
    john::TreeNode *bad_tree = new john::TreeNode(30, nullptr, nullptr);
    john::TreeNode *node_1 = new john::TreeNode(20, nullptr, nullptr);
    john::TreeNode *node_2 = new john::TreeNode(10, nullptr, nullptr);
    john::TreeNode *node_3 = new john::TreeNode(40, nullptr, nullptr);
    bad_tree->SetLeft(node_1);
    node_1->SetRight(node_2);
    bad_tree->SetRight(node_3);

    assert(!is_bst(bad_tree));

    // Delete
    tree.Delete(13); // No child
    std::cout << "Delete 13.\n";
    std::cout << "Tree items (in order): ";
    tree.PrintInOrder();

    tree.Delete(10); // One child
    std::cout << "Delete 10.\n";
    std::cout << "Tree items (in order): ";
    tree.PrintInOrder();

    tree.Delete(3); // Two children
    std::cout << "Delete 3.\n";
    std::cout << "Tree items (in order): ";
    tree.PrintInOrder();

    return 0;
}
