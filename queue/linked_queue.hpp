#ifndef LINKED_QUEUE_HPP
#define LINKED_QUEUE_HPP

#include "Node.hpp"

namespace john {

template <class T>
class LinkedQueue {
private:
    Node<T> *head_;
    Node<T> *tail_;
    
public:
    LinkedQueue();
    ~LinkedQueue();
    bool IsEmpty();
    void Enqueue(T value);
    T Dequeue();
    void Print();
};
   
}


#endif