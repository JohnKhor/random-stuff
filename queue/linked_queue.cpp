#include "linked_queue.hpp"
#include <iostream>

namespace john {

template <class T>
LinkedQueue<T>::LinkedQueue() : head_(nullptr), tail_(nullptr) {

}

template <class T>
LinkedQueue<T>::~LinkedQueue() {
    Node<T> *current = head_;
    Node<T> *next = nullptr;

    while(current) {
        next = current->GetNext();
        delete current;
        current = next;
    }
}

template <class T>
bool LinkedQueue<T>::IsEmpty() {
    return head_ == nullptr;
}

template <class T>
void LinkedQueue<T>::Enqueue(T value) {
    Node<T> *new_node = new Node<T>(value, nullptr);

    if (IsEmpty()) {
        head_ = tail_ = new_node;
    } else {
        tail_->SetNext(new_node);
        tail_ = new_node;
    }
}

template <class T>
T LinkedQueue<T>::Dequeue() {
    if (IsEmpty()) {
        std::cout << "Queue is empty.\n";
        exit(EXIT_FAILURE);
    }

    Node<T>* current = head_;
    T value = current->GetData();
    
    if (head_ == tail_) { // Queue has only one element
        tail_ = tail_->GetNext();
    }
    head_ = head_->GetNext();
    
    delete current;
    
    return value;
}

template <class T>
void LinkedQueue<T>::Print() {
    std::cout << "Items: ";
    Node<T> *current = head_;
    while(current) {
        std::cout << current->GetData() << " ";
        current = current->GetNext();
    }
    std::cout << "\n";
}

}
