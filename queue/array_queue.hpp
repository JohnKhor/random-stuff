#ifndef ARRAY_QUEUE_HPP
#define ARRAY_QUEUE_HPP

namespace john {

static const int QueueCapacity = 5;
static const int QueuePositions = QueueCapacity + 1;

template <class T>
class ArrayQueue {
private:
    int read_;
    int write_;
    T *array_;
    
public:
    ArrayQueue();
    ~ArrayQueue();
    bool IsEmpty();
    bool IsFull();
    void Enqueue(T value);
    T Dequeue();
    void Print();
};

}

#endif