#include "array_queue.hpp"
#include <iostream>

namespace john {

template <class T>
ArrayQueue<T>::ArrayQueue() : read_(0), write_(0) {
    array_ = new T[QueuePositions];
}

template <class T>
ArrayQueue<T>::~ArrayQueue() {
    delete[] array_;
}

template <class T>
bool ArrayQueue<T>::IsEmpty() {
    return read_ == write_;
}

template <class T>
bool ArrayQueue<T>::IsFull() {
    return read_ == (write_ + 1) % (QueuePositions);
}

template <class T>
void ArrayQueue<T>::Enqueue(T value) {
    if (IsFull()) {
        std::cout << "Queue is full.\n";
        exit(EXIT_FAILURE);
    }

    array_[write_] = value;
    write_ = (write_ + 1) % (QueuePositions);
}

template <class T>
T ArrayQueue<T>::Dequeue() {
    if (IsEmpty()) {
        std::cout << "Queue is empty.\n";
        exit(EXIT_FAILURE);
    }

    T value = array_[read_];
    read_ = (read_ + 1) % (QueuePositions);

    return value;
}

template <class T>
void ArrayQueue<T>::Print() {
    std::cout << "Items: ";
    for (int i = read_; i != write_; i = (i + 1) % QueuePositions) {
        std::cout << array_[i] << " ";
    }
    std::cout << "\n";
}

}
