#include "array_queue.hpp"
#include "array_queue.cpp"
#include "linked_queue.hpp"
#include "linked_queue.cpp"
#include <cassert>

void test_array_queue() {
    john::ArrayQueue<int> test;

    // Test isEmpty, enqueue and isFull
    assert(test.IsEmpty());
    test.Enqueue(0);
    test.Enqueue(1);
    assert(!test.IsFull());
    test.Enqueue(2);
    test.Enqueue(3);
    test.Enqueue(4);
    assert(!test.IsEmpty());
    assert(test.IsFull());

    test.Print();

    // Test dequeue
    assert(test.Dequeue() == 0);
    assert(test.Dequeue() == 1);
    assert(test.Dequeue() == 2);
    assert(test.Dequeue() == 3);
    assert(test.Dequeue() == 4);
    assert(test.IsEmpty());
}

void test_linked_queue() {
    john::LinkedQueue<int> test;

    // Test isEmpty and enqueue
    assert(test.IsEmpty());
    test.Enqueue(0);
    test.Enqueue(1);
    test.Enqueue(2);
    test.Enqueue(3);
    test.Enqueue(4);
    assert(!test.IsEmpty());

    test.Print();

    // Test dequeue
    assert(test.Dequeue() == 0);
    assert(test.Dequeue() == 1);
    assert(test.Dequeue() == 2);
    assert(test.Dequeue() == 3);
    assert(test.Dequeue() == 4);
    assert(test.IsEmpty());
}

int main() {
    test_array_queue();
    test_linked_queue();
    return 0;
}
