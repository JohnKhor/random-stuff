#include "vector.hpp"
#include "vector.cpp"
#include <cassert>

int main() {
    john::Vector<int> test;

    // Test isEmpty, size and push
    assert(test.IsEmpty());
    assert(test.Size() == 0);
    int itemsToPush = 10;
    for (int i = 0; i < itemsToPush; i++) {
        test.Push(i);
    } // 10 items
    assert(!test.IsEmpty());
    assert(test.Size() == itemsToPush);

    // Test at
    assert(test.At(0) == 0);
    assert(test.At(itemsToPush - 1) == itemsToPush - 1);

    // Test find
    assert(test.Find(5) == 5);
    assert(test.Find(50) == -1);

    // Test capacity, resize and pop
    assert(test.Capacity() == john::MinCapacity);
    
    // - Test increasing size
    for (int i = 0; i < 7; i++) {
        test.Push(i);
    } // 17 items, expand capacity to 32
    assert(test.Capacity() == john::MinCapacity * 2);
    
    for (int i = 0; i < 17; i++) {
        test.Push(i);
    } // 34 items, expand capacity to 64
    assert(test.Capacity() == john::MinCapacity * 4);
    
    // - Test decreasing size
    for (int i = 0; i < 19; i++) {
        test.Pop();
    } // 15 items, shrink capacity to 32 as 64 / 4 = 16 and 15 < 16
    assert(test.Capacity() == john::MinCapacity * 2);

    for (int i = 0; i < 15; i++) {
        test.Pop();
    } // 0 item, shrink capacity to default 16
    assert(test.Capacity() == john::MinCapacity);

    // Test insert and prepend
    test.Push(2);
    test.Push(4);
    test.Push(8); // 2 4 8
    test.Insert(2, 6); // 2 4 6 8
    assert(test.At(2) == 6);
    test.Prepend(0); // 0 2 4 6 8
    assert(test.At(0) == 0);
    test.Insert(5, 10); // 0 2 4 6 8 10
    assert(test.At(5) == 10);
    
    // Test delete
    test.Delete(3); // 0 2 4 8 10
    assert(test.At(3) == 8);

    // Test remove
    test.Push(4); // 0 2 4 8 10 4
    test.Remove(4); // 0 2 8 10
    test.Remove(8); // 0 2 10
    assert(test.At(0) == 0);
    assert(test.At(1) == 2);
    assert(test.At(2) == 10);

    test.Print();

    return 0;
}
