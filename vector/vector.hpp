#ifndef VECTOR_HPP
#define VECTOR_HPP 

namespace john {

static const int MinCapacity = 16;
static const int GrowthFactor = 2;
static const int ShrinkFactor = 4;

template <class T>
class Vector {
private:
    int size_;
    int capacity_;
    T* array_;
    bool InBounds(int index);
    void Resize(int new_size);

public:
    Vector();
    ~Vector();
    int Size();
    bool IsEmpty();
    int Capacity();
    T At(int index);
    void Push(T item);
    void Insert(int index, T item);
    void Prepend(T item);
    T Pop();
    void Delete(int index);
    void Remove(T item);
    int Find(T item);
    void Print();
};

}

#endif