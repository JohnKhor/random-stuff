#include "vector.hpp"
#include <iostream>

namespace john {

template <class T>
bool Vector<T>::InBounds(int index) {
    return index >= 0 && index < size_;
}

template <class T>
void Vector<T>::Resize(int new_size) {
    bool need_resize = false;
    if (new_size > capacity_) { // Expand size
        capacity_ = capacity_ * GrowthFactor;
        need_resize = true;
    } else if (new_size < capacity_ / ShrinkFactor) { // Shrink size
        capacity_ = capacity_ / 2;
        if (capacity_ < MinCapacity) {
            capacity_ = MinCapacity;
        }
        need_resize = true;
    }

    if (need_resize) {
        // Copy old elements
        int *new_array = new T[capacity_];
        for (int i = 0; i < size_; i++) {
            new_array[i] = array_[i];
        }
        
        // Delete old array
        delete[] array_;
        
        // Point to new array
        array_ = new_array;
    }
}

template <class T>
Vector<T>::Vector(): size_(0), capacity_(MinCapacity) {
    array_ = new T[capacity_];
}

template <class T>
Vector<T>::~Vector() {
    delete[] array_;
}

template <class T>
int Vector<T>::Size() {
    return size_;
}

template <class T>
bool Vector<T>::IsEmpty() {
    return size_ == 0;
}

template <class T>
int Vector<T>::Capacity() {
    return capacity_;
}

template <class T>
T Vector<T>::At(int index) {
    if (!InBounds(index)) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    return array_[index];
}

template <class T>
void Vector<T>::Push(T item) {
    Resize(size_ + 1);

    array_[size_] = item;
    ++size_;
}

template <class T>
void Vector<T>::Insert(int index, T item) {
    // Didn't use InBounds function. This is to allow insert at the back.
    if (index < 0 || index > size_) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Resize(size_ + 1);

    // Shift trailing elements to the right
    for (int i = size_ - 1; i >= index; i--) {
        array_[i + 1] = array_[i];  
    }

    array_[index] = item;
    ++size_;
}

template <class T>
void Vector<T>::Prepend(T item) {
    Insert(0, item);
}

template <class T>
T Vector<T>::Pop() {
    if (IsEmpty()) {
        std::cout << "Nothing to pop.\n";
        exit(EXIT_FAILURE);
    }

    Resize(size_ - 1);

    --size_;
}

template <class T>
void Vector<T>::Delete(int index) {
    if (!InBounds(index)) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Resize(size_ - 1);

    // Shift trailing elements to the left
    for (int i = index; i < size_ - 1; i++) {
        array_[i] = array_[i + 1];
    }

    --size_;
}

template <class T>
void Vector<T>::Remove(T item) {
    for (int i = 0; i < size_; i++) {
        if (array_[i] == item) {
            Delete(i);
            --i; // adjust since elements will be shifted to left
        }
    }
}

template <class T>
int Vector<T>::Find(T item) {
    int index = -1;

    for (int i = 0; i < size_; i++) {
        if (array_[i] == item) {
            index = i;
            break;
        }
    }

    return index;
}

template <class T>
void Vector<T>::Print() {
    std::cout << "Size: " << size_ << "\n";
    std::cout << "Capacity: " << capacity_ << "\n";
    std::cout << "Items: ";
    for (int i = 0; i < size_; i++) {
        std::cout << array_[i] << " ";
    }
    std::cout << "\n";
}

}
