#ifndef PRIORITY_QUEUE_HPP
#define PRIORITY_QUEUE_HPP

#include "entry.hpp"

namespace john {

class PriorityQueue {
private:
    int capacity_;
    int size_;
    Entry *array_;
    int Parent(int index);
    int LeftChild(int index);
    int RightChild(int index);
    void Swap(int first, int second);
    void HeapifyUp(int index);
    void HeapifyDown(int index);

public:
    PriorityQueue(int capacity);
    ~PriorityQueue();
    int Size();
    bool IsEmpty();
    void Insert(int key, std::string value);
    void Remove(int index);
    Entry GetMax();
    Entry ExtractMax();
    void Print();
};

}
#endif