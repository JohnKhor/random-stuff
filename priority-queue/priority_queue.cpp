#include "priority_queue.hpp"
#include <iostream>

namespace john {

int PriorityQueue::Parent(int index) {
    return (index - 1) / 2;
}

int PriorityQueue::LeftChild(int index) {
    return 2 * index + 1;
}

int PriorityQueue::RightChild(int index) {
    return 2 * index + 2;
}

void PriorityQueue::Swap(int first, int second) {
    int temp_key = array_[first].GetKey();
    std::string temp_value = array_[first].GetValue();

    array_[first].SetKey(array_[second].GetKey());
    array_[first].SetValue(array_[second].GetValue());

    array_[second].SetKey(temp_key);
    array_[second].SetValue(temp_value);
}

PriorityQueue::PriorityQueue(int capacity) : capacity_(capacity), size_(0) {
    if (capacity_ <= 0) {
        std::cout << "ERROR: Unable to build priority queue of size " << capacity_ << "\n";
        exit(EXIT_FAILURE);
    }

    array_ = new Entry[capacity_];
}

PriorityQueue::~PriorityQueue() {
    delete[] array_;
}

int PriorityQueue::Size() {
    return size_;
}

bool PriorityQueue::IsEmpty() {
    return size_ == 0;
}

void PriorityQueue::HeapifyUp(int index) {
    int parent;

    // Stops when reach the root
    while(index > 0) {
        parent = Parent(index);
        
        // Compare with parent, swap if necessary
        if (array_[parent].GetKey() < array_[index].GetKey()) {
            Swap(parent, index);
        } else {
            break;
        }

        index = parent;
    }
}

void PriorityQueue::Insert(int key, std::string value) {
    if (size_ == capacity_) {
        std::cout << "ERROR: Priority queue is full.\n";
        exit(EXIT_FAILURE);
    }

    array_[size_].SetKey(key);
    array_[size_].SetValue(value);

    ++size_;

    HeapifyUp(size_ - 1);
}

void PriorityQueue::HeapifyDown(int index) {
    int largest = index;

    int left = LeftChild(index);
    if (left < size_ && array_[left].GetKey() > array_[largest].GetKey()) {
        largest = left;
    }

    int right = RightChild(index);
    if (right < size_ && array_[right].GetKey() > array_[largest].GetKey()) {
        largest = right;
    }

    if (largest != index) {
        Swap(index, largest);
        HeapifyDown(largest);
    }
}

void PriorityQueue::Remove(int index) {
    if (index < 0 || index >= size_) {
        std::cout << "ERROR: Index is out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Swap(index, size_ - 1); // Swap with rightmost leaf node
    --size_; // Remove rightmost leaf node
    
    HeapifyDown(index);
}

Entry PriorityQueue::GetMax() {
    if (IsEmpty()) {
        std::cout << "ERROR: Priority queue is empty.\n";
        exit(EXIT_FAILURE);
    }

    Entry max;
    max.SetKey(array_[0].GetKey());
    max.SetValue(array_[0].GetValue());

    return max;
}

Entry PriorityQueue::ExtractMax() {
    if (IsEmpty()) {
        std::cout << "ERROR: Priority queue is empty.\n";
        exit(EXIT_FAILURE);
    }

    Entry max;
    max.SetKey(array_[0].GetKey());
    max.SetValue(array_[0].GetValue());

    Remove(0);

    return max;
}

void PriorityQueue::Print() {
    std::cout << "Index\t\tKey\t\tValue\n";
    for (int i = 0; i < size_; i++) {
        std::cout << i << "\t\t" << array_[i].GetKey() << "\t\t" << array_[i].GetValue() << "\n";
    }
}

}
