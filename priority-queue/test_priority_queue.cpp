#include "priority_queue.hpp"
#include "priority_queue.cpp"
#include "heap_sort.cpp"
#include <cassert>

int main() {
    john::PriorityQueue pq(10);

    // IsEmpty, Size and Insert
    assert(pq.IsEmpty());
    assert(pq.Size() == 0);

    //         100
    //       /    \
    //     70     80
    //    /  \   /  \
    //   42  60 10  30
    //  / \  /
    // 5  7 1
    pq.Insert(100, "One hundred");
    pq.Insert(42, "Forty-two");
    pq.Insert(30, "Thirty");
    pq.Insert(60, "Sixty");
    pq.Insert(70, "Seventy");
    pq.Insert(10, "Ten");
    pq.Insert(80, "Eighty");
    pq.Insert(5, "Five");
    pq.Insert(7, "Seven");
    pq.Insert(1, "One");

    assert(!pq.IsEmpty());
    assert(pq.Size() == 10);

    pq.Print();

    // GetMax and ExtractMax
    john::Entry max = pq.GetMax();
    assert(max.GetKey() == 100);
    assert(max.GetValue() == "One hundred");

    max = pq.ExtractMax();
    assert(max.GetKey() == 100);
    assert(max.GetValue() == "One hundred");

    max = pq.ExtractMax();
    assert(max.GetKey() == 80);
    assert(max.GetValue() == "Eighty");

    // Remove
    pq.Remove(3); // key 42, has left child
    pq.Remove(2); // key 30, has two children
    pq.Remove(4); // key 7, has no child
    assert(pq.Size() == 5);

    pq.Print();

    // Heap sort
    const int SIZE = 8;
    int to_sort[SIZE] = {5, 3, 1, 7, 4, 2, 8, 6};
    int sorted[SIZE] = {1, 2, 3, 4, 5, 6, 7, 8};

    john::build_heap(to_sort, SIZE);
    john::heap_sort(to_sort, SIZE);
    bool is_sorted = true;
    for (int i = 0; i < SIZE; i++) {
        if (to_sort[i] != sorted[i]) {
            is_sorted = false;
            break;
        }
    }
    assert(is_sorted);

    return 0;
}
