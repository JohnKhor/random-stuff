namespace john {

void swap(int array[], int first, int second) {
    int temp = array[first];
    array[first] = array[second];
    array[second] = temp;
}

void heapify_down(int array[], int length, int index) {
    int largest = index;

    int left = 2 * index + 1;
    if (left < length && array[left] > array[largest]) {
        largest = left;
    }

    int right = 2 * index + 2;
    if (right < length && array[right] > array[largest]) {
        largest = right;
    }

    if (largest != index) {
        swap(array, index, largest);
        heapify_down(array, length, largest);
    }    
}

void build_heap(int array[], int length) {
    // Start one level above leaf nodes as leaf nodes are already heaps
    for (int i = (length / 2) - 1; i >= 0; i--) {
        heapify_down(array, length, i);
    }
}

void heap_sort(int array[], int length) {
    int size = length - 1;
    while(size > 0) {
        swap(array, 0, size); // Swap max node with rightmost leaf node
        --size;
        heapify_down(array, size, 0);
    }
}

}
