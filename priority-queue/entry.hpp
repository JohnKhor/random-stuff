#ifndef ENTRY_HPP
#define ENTRY_HPP

#include <string>

namespace john {

class Entry {
private:
    int key_;
    std::string value_;

public:
    Entry() : key_(0), value_("") {}
    int GetKey() { return key_; }
    void SetKey(int key) { key_ = key; }
    std::string GetValue() { return value_; }
    void SetValue(std::string value) { value_ = value; }
};

}

#endif