#include "hash_table.hpp"

namespace john {

int HashTable::Hash(std::string key) {
    int hash = 0;
    int key_length = key.length();
    for (int i = 0; i < key_length; i++) {
        hash = hash * 31 + (int)key[i];
    }
    return hash % capacity_;
}

HashTable::HashTable(int capacity) : capacity_(capacity), debug_mode_(false) {
    array_ = new Pair[capacity_];
}

HashTable::~HashTable() {
    delete[] array_;
}

void HashTable::Add(std::string key, std::string value) {
    int index = Hash(key);
    int original_index = index;
    int collisions = 0;

    // Continue probing until found a spot with null flag or deleted flag.
    while(!array_[index].IsNull() && !array_[index].IsDeleted()) {
        ++collisions;

        index = (index + 1) % capacity_;

        // Also stop probing if already search through the whole table
        if (index == original_index) { 
            std::cout << "ERROR: Table is full.\n";
            exit(EXIT_FAILURE);
        }
    }

    array_[index].SetKey(key);
    array_[index].SetValue(value);

    if (debug_mode_) {
        std::cout << "Inserting (" << key << ", " << value << ") took " << collisions << " collisons.\n";
    }
}

bool HashTable::Exists(std::string key) {
    int index = Hash(key);
    int original_index = index;

    // Continue probing if found a spot without the same key or with deleted flag.
    // Stop probing if found a spot with the same key or null flag.
    while((array_[index].GetKey() != key && !array_[index].IsNull()) || array_[index].IsDeleted()) {
        index = (index + 1) % capacity_;

        if (index == original_index) { 
            break;
        }
    }

    return array_[index].GetKey() == key;
}

std::string HashTable::Get(std::string key) {
    int index = Hash(key);
    int original_index = index;

    while((array_[index].GetKey() != key && !array_[index].IsNull()) || array_[index].IsDeleted()) {
        index = (index + 1) % capacity_;

        if (index == original_index) { 
            break;
        }
    }

    if (array_[index].GetKey() == key) {
        return array_[index].GetValue();
    } else {
        return NULL_FLAG;
    }
}

void HashTable::Remove(std::string key) {
    int index = Hash(key);
    int original_index = index;

    while((array_[index].GetKey() != key && !array_[index].IsNull()) || array_[index].IsDeleted()) {
        index = (index + 1) % capacity_;

        if (index == original_index) { 
            break;
        }
    }

    if (array_[index].GetKey() == key) {
        array_[index].SetAsDeleted();
    }
}

void HashTable::Print() {
    std::cout << "INDEX\tKEY\t\tVALUE\n";
    for (int i = 0; i < capacity_; i++) {
        std::cout << i << "\t" << array_[i].GetKey() << "\t\t" << array_[i].GetValue() << "\n"; 
    }
}

void HashTable::SetDebugMode(bool debug_mode) {
    debug_mode_ = debug_mode;
}

}
