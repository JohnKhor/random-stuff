#include "hash_table.hpp"
#include "hash_table.cpp"
#include <cassert>

int main() {
    john::HashTable test(10);
    test.SetDebugMode(true);

    // Test add and exists
    test.Add("California", "Sacramento");
    assert(test.Exists("California"));
    assert(!test.Exists("Alabama"));

    // Test probing
    test.Add("Florida", "Tallahassee");
    test.Add("Georgia", "Atlanta");
    test.Add("Kentucky", "Frankfort");
    test.Add("Michigan", "Lansing"); // 6 collisons
    test.Add("Nebraska", "Lincoln");
    test.Add("New York", "Albany"); // 2 collisons
    test.Add("Texas", "Austin"); // 1 collison
    test.Add("Washington", "Olympia"); // 5 collisons

    // Test get
    assert(test.Get("Georgia") == "Atlanta");
    assert(test.Get("New York") == "Albany");
    assert(test.Get("Alabama") == john::NULL_FLAG);

    // Test remove
    test.Remove("Kentucky");
    assert(!test.Exists("Kentucky"));

    test.Print();

    return 0;
}
