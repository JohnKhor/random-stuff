#ifndef HASH_TABLE_HPP
#define HASH_TABLE_HPP

#include "pair.hpp"

namespace john {

class HashTable {
private:
    int capacity_;
    Pair *array_;
    bool debug_mode_;
    int Hash(std::string key);

public:
    HashTable(int capacity);
    ~HashTable();
    void Add(std::string key, std::string value);
    bool Exists(std::string key);
    std::string Get(std::string key);
    void Remove(std::string key);
    void Print();
    void SetDebugMode(bool debug_mode);
};

}

#endif