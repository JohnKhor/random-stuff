#ifndef PAIR_HPP
#define PAIR_HPP

#include <iostream>

namespace john {

static const std::string NULL_FLAG = "<NULL>";
static const std::string DELETED_FLAG = "<DELETED>";

class Pair {
private:
    std::string key_;
    std::string value_;

public:
    Pair() : key_(NULL_FLAG), value_("") { }
    std::string GetKey() { return key_; }
    void SetKey(std::string key) { key_ = key; }
    std::string GetValue() { return value_; }
    void SetValue(std::string value) { value_ = value; }
    bool IsNull() { return key_ == NULL_FLAG; }
    bool IsDeleted() { return key_ == DELETED_FLAG; }
    void SetAsDeleted() { key_ = DELETED_FLAG; value_ = ""; }
};

}

#endif