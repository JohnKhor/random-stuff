#include "list.hpp"
#include <iostream>

namespace john {

template <class T>
bool List<T>::InBounds(int index) {
    return index >= 0 and index < size_;
}

template <class T>
List<T>::List() : head_(nullptr), size_(0) {

}

template <class T>
List<T>::~List() {
    Node<T> *current = head_;
    Node<T> *next = nullptr;
    
    while(current) {
        next = current->GetNext();
        delete current;
        current = next;
    }

    size_ = 0;
}

template <class T>
int List<T>::Size() {
    return size_;
}

template <class T>
bool List<T>::IsEmpty() {
    return head_ == nullptr;
}

template <class T>
T List<T>::ValueAt(int index) {
    if (!InBounds(index)) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Node<T> *current = head_;
    
    for (int i = 0; i < index; i++) {
        current = current->GetNext();
    }

    return current->GetData();
}

template <class T>
void List<T>::PushFront(T value) {
    Node<T>* new_node = new Node<T>(value, head_);
    head_ = new_node;
    ++size_;
}

template <class T>
T List<T>::PopFront() {
    if (IsEmpty()) {
        std::cout << "Nothing to pop.\n";
        exit(EXIT_FAILURE);
    }

    Node<T> *current = head_;
    T data = current->GetData();

    head_ = head_->GetNext();
    
    delete current;
    --size_;
    
    return data;
}

template <class T>
void List<T>::PushBack(T value) {
    Node<T> *new_node = new Node<T>(value, nullptr);
    Node<T> *current = head_;
    
    if (current == nullptr) {
        head_ = new_node;
    } else {
        while(current->GetNext()) {
            current = current->GetNext();
        }
        current->SetNext(new_node);
    }
    
    ++size_;
}

template <class T>
T List<T>::PopBack() {
    if (IsEmpty()) {
        std::cout << "Nothing to pop.\n";
        exit(EXIT_FAILURE);
    }

    Node<T> *current = head_;
    Node<T> *prev = nullptr;
    
    while(current->GetNext()) {
        prev = current;
        current = current->GetNext();
    }
    
    if (prev == nullptr) {
        head_ = nullptr;
    } else {
        prev->SetNext(nullptr);
    }

    T value = current->GetData();

    delete current;
    --size_;

    return value;
}

template <class T>
T List<T>::Front() {
    if (IsEmpty()) {
        std::cout << "List is empty.\n";
        exit(EXIT_FAILURE);
    }

    return head_->GetData();
}

template <class T>
T List<T>::Back() {
    if (IsEmpty()) {
        std::cout << "List is empty.\n";
        exit(EXIT_FAILURE);
    }

    Node<T> *current = head_;
    
    while(current->GetNext()) {
        current = current->GetNext();
    }
    
    return current->GetData();
}

template <class T>
void List<T>::Insert(int index, T value) {
    // Didn't use InBounds function. This is to allow insert at the back.

    Node<T> *current = head_;
    Node<T> *prev = nullptr;
    
    int i;
    for (i = 0; i < index && current; i++) {
        prev = current;
        current = current->GetNext();
    }

    if (i != index) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Node<T> *new_node = new Node<T>(value, current);
    
    if (prev == nullptr) {
        head_ = new_node;
    } else {
        prev->SetNext(new_node);
    }
    
    ++size_;
}

template <class T>
void List<T>::Erase(int index) {
    if (!InBounds(index)) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Node<T> *current = head_;
    Node<T> *prev = nullptr;
    
    for (int i = 0; i < index; i++) {
        prev = current;
        current = current->GetNext();
    }

    if (prev == nullptr) {
        head_ = head_->GetNext();
    } else {
        prev->SetNext(current->GetNext());
    }
    
    delete current;
    --size_;
}

template <class T>
T List<T>::NthValueFromEnd(int n) {
    if (!InBounds(n)) {
        std::cout << "Index out of bounds.\n";
        exit(EXIT_FAILURE);
    }

    Node<T>* current = head_;
    
    for (int i = 0; i < size_ - n - 1; i++) {
        current = current->GetNext();
    }

    return current->GetData();
}

template <class T>
void List<T>::Reverse() {
    if (size_ <= 1) {
        return;
    }

    Node<T> *current = head_;
    Node<T> *prev = nullptr;
    Node<T> *next = nullptr;
    
    while(current != nullptr) {
        next = current->GetNext();
        current->SetNext(prev);
        prev = current;
        current = next;
    }

    head_ = prev;
}

template <class T>
void List<T>::RemoveValue(T value) {
    Node<T> *current = head_;
    Node<T> *prev = nullptr;

    while(current) {
        if (current->GetData() == value) {
            if (prev == nullptr) {
                head_ = head_->GetNext();
            } else {
                prev->SetNext(current->GetNext());
            }

            delete current;
            --size_;
            
            break;
        }

        prev = current;
        current = current->GetNext();
    }
}

template <class T>
void List<T>::Print() {
    std::cout << "Size: " << size_ << "\n";
    std::cout << "Items: ";
    Node<T> *current = head_;
    while(current) {
        std::cout << current->GetData() << " ";
        current = current->GetNext();
    }
    std::cout << "\n"; 
}

}
