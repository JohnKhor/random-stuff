#ifndef LIST_HPP
#define LIST_HPP

#include "node.hpp"

namespace john {

template <class T>
class List {
private:
    Node<T> *head_;
    int size_;
    bool InBounds(int index);

public:
    List();
    ~List();
    int Size();
    bool IsEmpty();
    T ValueAt(int index);
    void PushFront(T value);
    T PopFront();
    void PushBack(T value);
    T PopBack();
    T Front();
    T Back();
    void Insert(int index, T value);
    void Erase(int index);
    T NthValueFromEnd(int n);
    void Reverse();
    void RemoveValue(T value);
    void Print();
};

}

#endif