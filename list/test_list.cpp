#include "list.hpp"
#include "list.cpp"
#include <cassert>

int main() {
    john::List<int> test;

    // Test isEmpty, size, pushFront and valueAt
    assert(test.IsEmpty());
    assert(test.Size() == 0);
    test.PushFront(4);
    test.PushFront(5);
    test.PushFront(6); // 6 5 4
    assert(test.ValueAt(0) == 6);
    assert(test.ValueAt(1) == 5);
    assert(test.ValueAt(2) == 4);
    assert(!test.IsEmpty());
    assert(test.Size() == 3);
    
    // Test popFront
    assert(test.PopFront() == 6);
    assert(test.PopFront() == 5);
    assert(test.PopFront() == 4);

    // Test pushBack, front and back
    test.PushBack(4);
    test.PushBack(5);
    test.PushBack(6); // 4 5 6
    assert(test.ValueAt(0) == 4);
    assert(test.ValueAt(1) == 5);
    assert(test.ValueAt(2) == 6);
    assert(test.Front() == 4);
    assert(test.Back() == 6);

    // Test popBack
    assert(test.PopBack() == 6);
    assert(test.PopBack() == 5);
    assert(test.PopBack() == 4);

    // Test insert
    test.Insert(0, 10); // Insert at front. 10
    test.Insert(0, 20); // Insert at front. 20 10
    test.Insert(1, 30); // Insert at middle. 20 30 10
    test.Insert(3, 40); // Insert at back. 20 30 10 40
    test.Insert(4, 50); // Insert at back. 20 30 10 40 50
    assert(test.ValueAt(0) == 20);
    assert(test.ValueAt(1) == 30);
    assert(test.ValueAt(2) == 10);
    assert(test.ValueAt(3) == 40);
    assert(test.ValueAt(4) == 50);

    // Test nthValueFromEnd
    assert(test.NthValueFromEnd(1) == 40);
    assert(test.NthValueFromEnd(3) == 30);

    // Test erase
    test.Erase(2); // 20 30 40 50
    test.Erase(0); // 30 40 50
    test.Erase(2); // 30 40
    assert(test.ValueAt(0) == 30);
    assert(test.ValueAt(1) == 40);

    // Test removeValue
    test.Insert(1, 50);
    test.Insert(1, 60);
    test.Insert(1, 70);
    test.RemoveValue(30); // Remove front
    test.RemoveValue(40); // Remove back
    test.RemoveValue(60); // Remove middle
    test.RemoveValue(50);
    test.RemoveValue(70);
    assert(test.IsEmpty());

    // Test reverse
    test.PushBack(0);
    test.PushBack(1);
    test.PushBack(2);
    test.Reverse();
    assert(test.ValueAt(0) == 2);
    assert(test.ValueAt(1) == 1);
    assert(test.ValueAt(2) == 0);

    test.Print();

    return 0;
}
