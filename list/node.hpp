#ifndef NODE_HPP
#define NODE_HPP 

namespace john {

template <class T>
class Node {
private:
    T data_;
    Node<T> *next_;
public:
    Node(T data, Node<T> *next): data_(data), next_(next) {}
    T GetData() { return data_; }
    void SetData(T data) { data_ = data; }
    Node<T>* GetNext() { return next_; }
    void SetNext(Node<T>* next) { next_ = next; }
};

}

#endif