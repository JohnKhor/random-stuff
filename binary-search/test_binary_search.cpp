#include "binary_search.cpp"
#include <cassert>

void test_binary_search() {
    const int SIZE_1 = 1;
    int test_1[SIZE_1] = {5};
    
    assert(john::binary_search_iterative(5, test_1, SIZE_1) == 0);
    assert(john::binary_search_iterative(1, test_1, SIZE_1) == -1);
    
    assert(john::binary_search_recursive(5, test_1, SIZE_1) == 0);
    assert(john::binary_search_recursive(1, test_1, SIZE_1) == -1);
    
    const int SIZE_2 = 2;
    int test_2[SIZE_2] = {5, 6};
    
    assert(john::binary_search_iterative(5, test_2, SIZE_2) == 0);
    assert(john::binary_search_iterative(6, test_2, SIZE_2) == 1);
    assert(john::binary_search_iterative(1, test_2, SIZE_2) == -1);
    
    assert(john::binary_search_recursive(5, test_2, SIZE_2) == 0);
    assert(john::binary_search_recursive(6, test_2, SIZE_2) == 1);
    assert(john::binary_search_recursive(1, test_2, SIZE_2) == -1);
    
    const int SIZE_3 = 3;
    int test_3[SIZE_3] = {5, 6, 7};
    
    assert(john::binary_search_iterative(5, test_3, SIZE_3) == 0);
    assert(john::binary_search_iterative(6, test_3, SIZE_3) == 1);
    assert(john::binary_search_iterative(7, test_3, SIZE_3) == 2);
    assert(john::binary_search_iterative(1, test_3, SIZE_3) == -1);
    
    assert(john::binary_search_recursive(5, test_3, SIZE_3) == 0);
    assert(john::binary_search_recursive(6, test_3, SIZE_3) == 1);
    assert(john::binary_search_recursive(7, test_3, SIZE_3) == 2);
    assert(john::binary_search_recursive(1, test_3, SIZE_3) == -1);

    const int SIZE_4 = 9;
    int test_4[SIZE_4] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    
    assert(john::binary_search_iterative(5, test_4, SIZE_4) == 4);
    assert(john::binary_search_iterative(2, test_4, SIZE_4) == 1);
    assert(john::binary_search_iterative(8, test_4, SIZE_4) == 7);
    assert(john::binary_search_iterative(10, test_4, SIZE_4) == -1);

    assert(john::binary_search_recursive(5, test_4, SIZE_4) == 4);
    assert(john::binary_search_recursive(2, test_4, SIZE_4) == 1);
    assert(john::binary_search_recursive(8, test_4, SIZE_4) == 7);
    assert(john::binary_search_recursive(10, test_4, SIZE_4) == -1);

    const int SIZE_5 = 10;
    int test_5[SIZE_5] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    
    assert(john::binary_search_iterative(5, test_5, SIZE_5) == 4);
    assert(john::binary_search_iterative(2, test_5, SIZE_5) == 1);
    assert(john::binary_search_iterative(9, test_5, SIZE_5) == 8);
    assert(john::binary_search_iterative(11, test_5, SIZE_5) == -1);

    assert(john::binary_search_recursive(5, test_5, SIZE_5) == 4);
    assert(john::binary_search_recursive(2, test_5, SIZE_5) == 1);
    assert(john::binary_search_recursive(9, test_5, SIZE_5) == 8);
    assert(john::binary_search_recursive(11, test_5, SIZE_5) == -1);
}

void test_occurrence() {
    const int SIZE = 9;
    int test[SIZE] = {1, 2, 2, 3, 3, 3, 4, 4, 5};

    assert(john::first_occurrence(1, test, SIZE) == 0);
    assert(john::count_occurrences(1, test, SIZE) == 1);
    
    assert(john::first_occurrence(2, test, SIZE) == 1);
    assert(john::last_occurrence(2, test, SIZE) == 2);
    assert(john::count_occurrences(2, test, SIZE) == 2);
    
    assert(john::first_occurrence(3, test, SIZE) == 3);
    assert(john::last_occurrence(3, test, SIZE) == 5);
    assert(john::count_occurrences(3, test, SIZE) == 3);
    
    assert(john::first_occurrence(4, test, SIZE) == 6);
    assert(john::last_occurrence(4, test, SIZE) == 7);
    assert(john::count_occurrences(4, test, SIZE) == 2);

    assert(john::first_occurrence(8, test, SIZE) == -1);
    assert(john::last_occurrence(8, test, SIZE) == -1);
    assert(john::count_occurrences(8, test, SIZE) == 0);
}

int main() {
    test_binary_search();
    test_occurrence();
    return 0;
}
