namespace john {

int binary_search_iterative(int target, int array[], int length) {
    int low = 0;
    int high = length - 1;

    while(low <= high) {
        int mid = (low + high) / 2;
        
        if (array[mid] > target) {
            high = mid - 1;
        } else if (array[mid] < target) {
            low = mid + 1;
        } else {
            return mid;
        }
    }

    return -1;
}

int binary_search_recursive(int target, int array[], int low, int high) {
    if (low > high) {
        return -1;
    }

    int mid = (low + high) / 2;
    
    if (array[mid] > target) {
        binary_search_recursive(target, array, low, high - 1);
    } else if (array[mid] < target) {
        binary_search_recursive(target, array, mid + 1, high);
    } else {
        return mid;
    }
}

int binary_search_recursive(int target, int array[], int length) {
    return binary_search_recursive(target, array, 0, length - 1);
}

int first_occurrence(int target, int array[], int length) {
    int low = 0;
    int high = length - 1;

    while(low <= high) {
        int mid = (low + high) / 2;

        if (array[mid] == target && (mid == 0 || array[mid - 1] != target)) {
            return mid;
        } else if (array[mid] < target) {
            low = mid + 1;
        } else {
            // Also handles the case where array[mid] = target = array[mid - 1]  
            high = mid - 1;
        }
    }

    return -1;
}

int last_occurrence(int target, int array[], int length) {
    int low = 0;
    int high = length - 1;

    while(low <= high) {
        int mid = (low + high) / 2;
        
        if (array[mid] == target && (mid == length - 1 || array[mid + 1] != target)) {
            return mid;
        } else if (array[mid] > target) {
            high = mid - 1;
        } else {
            // Also handles the case where array[mid] = target = array[mid + 1]
            low = mid + 1;
        }
    }

    return -1;    
}

int count_occurrences(int target, int array[], int length) {
    int first = first_occurrence(target, array, length);
    if (first == -1) {
        return 0;
    }
    int last = last_occurrence(target, array, length);
    return last - first + 1;
}

}
